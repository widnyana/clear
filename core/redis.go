package core

import (
	"strconv"
	"time"

	"github.com/gomodule/redigo/redis"
	conf "gitlab.com/widnyana/clear/config"
)

var redisPool *redis.Pool

// RedisSetup configure redis connection and its pool
func RedisSetup() error {
	redisPool = &redis.Pool{
		MaxIdle:     conf.RedisConf.MaxIdle,
		MaxActive:   conf.RedisConf.MaxActive,
		IdleTimeout: conf.RedisConf.IdleTimeout,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", conf.RedisConf.Host+":"+strconv.Itoa(conf.RedisConf.Port))
			if err != nil {
				return nil, err
			}
			// Validate password
			if conf.RedisConf.Password != "" {
				if _, err := c.Do("AUTH", conf.RedisConf.Password); err != nil {
					c.Close()
					return nil, err
				}
			}
			// Choose database
			if _, err := c.Do("SELECT", conf.RedisConf.DBNum); err != nil {
				c.Close()
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
	return nil
}

// RedisSet method
func RedisSet(key string, data string, seconds int) error {
	conn := GetRedisConn().Get()
	defer conn.Close()

	_, err := conn.Do("SET", key, data)
	if err != nil {
		return err
	}

	_, err = conn.Do("EXPIRE", key, seconds)
	if err != nil {
		return err
	}
	return nil
}

// Exists method
func Exists(key string) bool {
	conn := GetRedisConn().Get()
	defer conn.Close()

	exists, err := redis.Bool(conn.Do("EXISTS", key))
	if err != nil {
		return false
	}
	return exists
}

// RedisGet method
func RedisGet(key string) (string, error) {
	conn := GetRedisConn().Get()
	defer conn.Close()

	reply, err := redis.String(conn.Do("GET", key))
	if err != nil && err != redis.ErrNil {
		return "", err
	}
	if err == redis.ErrNil {
		return "", nil
	}
	return reply, nil
}

// GetRedisConn provide redis connection
func GetRedisConn() *redis.Pool {
	return redisPool
}

// RedisDel method
func RedisDel(key string) (bool, error) {
	conn := GetRedisConn().Get()
	defer conn.Close()

	return redis.Bool(conn.Do("DEL", key))
}

// RedisDelLike method
func RedisDelLike(key string) error {
	conn := GetRedisConn().Get()
	defer conn.Close()

	keys, err := redis.Strings(conn.Do("KEYS", "*"+key+"*"))
	if err != nil {
		return err
	}

	for _, key := range keys {
		_, err := RedisDel(key)
		if err != nil {
			return err
		}
	}
	return nil
}
