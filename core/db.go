package core

// DEPRECATED!


import (
	"github.com/jmoiron/sqlx"

	// go mysql driver
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/widnyana/clear/config"
)

// DBSetup configure firstime database connection
func DBSetup() error {
	dsn := config.MysqlConf.CreateDSN()
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return err
	}

	defer db.Close()

	// connection pooling stuff
	db.SetMaxOpenConns(config.MysqlConf.MaxOpenConn)
	db.SetMaxIdleConns(config.MysqlConf.MaxIdleConn)
	db.SetConnMaxLifetime(config.MysqlConf.ConnMaxLifetime)

	return err
}

// DBGetConnection return mysql database connection
func DBGetConnection() (*sqlx.DB, error) {
	db, err := sqlx.Connect("mysql", config.MysqlConf.CreateDSN())
	db.SetMaxOpenConns(config.MysqlConf.MaxOpenConn)
	db.SetMaxIdleConns(config.MysqlConf.MaxIdleConn)
	db.SetConnMaxLifetime(config.MysqlConf.ConnMaxLifetime)

	return db, err
}
