DIST := dist
SERVICE ?= clear-server

GOFMT ?= gofmt "-s"
SHASUM ?= shasum -a 256
GO ?= go
TARGETS ?= linux darwin windows
ARCHS ?= amd64 386
BUILD_DATE ?= $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
PACKAGES ?= $(shell $(GO) list ./... | grep -v integrations)
GOFILES := $(shell find . -name "*.go" -type f)
TAGS ?= "sqlite sqlite_unlock_notify"


ifneq ($(shell uname), Darwin)
	EXTLDFLAGS = -extldflags "-static" $(null)
else
	EXTLDFLAGS =
endif

VERSION ?= $(shell git describe --tags --always | sed 's/-/+/' | sed 's/^v//')

LDFLAGS ?= -X gitlab.com/widnyana/clear/core.Version=$(VERSION) -X gitlab.com/widnyana/clear/core.BuildDate=$(BUILD_DATE)


all: build
.PHONY: vendor
vendor:
	GO111MODULE=on $(GO) mod tidy && GO111MODULE=on $(GO) mod vendor

.PHONY: fmt
fmt:
	$(GOFMT) -w $(GOFILES)

.PHONY: build
build:
	$(GO) build -v -tags '$(TAGS)' -ldflags '$(EXTLDFLAGS)-s -w $(LDFLAGS)' -o bin/$(SERVICE)-tmp .
	upx --best -o bin/$(SERVICE) bin/$(SERVICE)-tmp
	rm -fr bin/$(SERVICE)-tmp


clean_dist:
	rm -fr tmp
	rm -fr clear
	rm -fr bin/*


clean: clean_dist
	$(GO) clean -modcache -cache -x -i ./...
