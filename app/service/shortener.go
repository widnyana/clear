package service

import (
	"fmt"

	gonanoid "github.com/matoous/go-nanoid"
	"gitlab.com/widnyana/clear/app/models"
)

type (
	// ShortenerService provice action for interacting with url table
	ShortenerService struct{}
)

var (
	dict        = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	defaultSize = 6
)

// GetURLID func to short the input URL
func GetURLID(param ...int) string {
	var size int
	var err error
	if len(param) == 0 {
		size = defaultSize
	} else {
		size = param[0]
	}

	urlid, err := gonanoid.Generate(dict, size)
	for err != nil {
		urlid, err = gonanoid.Generate(dict, size)
	}
	return urlid
}

// Retrieve return true when shourt url already used
func (sc ShortenerService) Retrieve(short, url string, owner uint) (state bool, res models.URLModel) {
	var query string
	var param []interface{}
	if owner == 0 {
		query = "(url = ? OR short_url = ?)"
		param = []interface{}{
			url,
			short,
		}
	} else {
		query = "owner_id = ? AND (url = ? OR short_url = ?)"
		param = []interface{}{
			owner,
			url,
			short,
		}
	}

	db := models.DB.Where(query, param...).Find(&res)

	if db.RowsAffected > 0 {
		state = true
	}
	return state, res
}

// Store will create and save shortened URL
func (sc ShortenerService) Store(url, short string, owner uint) (res models.URLModel, err error) {
	if len(short) < 1 {
		short = GetURLID(4)
	}

	exist, val := sc.Retrieve(short, url, owner)
	if exist {
		if val.URL == url {
			return val, nil
		}

		if val.URL != url && val.ShortURL == short {
			return res, fmt.Errorf("Name \"%s\" already used", short)
		}
	}

	m := models.URLModel{
		URL:      url,
		ShortURL: short,
		OwnerID:  owner,
	}

	state := models.DB.Create(&m)
	if state.Error != nil {
		return res, state.Error
	}

	res = m
	return res, err
}

// Delete short url data
func (sc ShortenerService) Delete(name string, ownerID uint) error {
	var res models.URLModel
	query := "short_url = ? AND owner_id = ?"
	db := models.DB.Where(query, name, ownerID).First(&res)

	if db.RowsAffected > 0 {
		state := db.Delete(&res)
		return state.Error
	}

	return fmt.Errorf("No Record to Delete")
}
