package service

import (
	"time"

	goJWT "github.com/dgrijalva/jwt-go"
	"gitlab.com/widnyana/clear/app/models"
	"gitlab.com/widnyana/clear/config"
	"gitlab.com/widnyana/clear/core"
)

// AuthService contains action for auth with jwt
type AuthService struct {
	User *models.UserModel
}

// GenerateToken ...
func (as *AuthService) GenerateToken(user models.UserModel) (string, error) {
	jwtInstance := core.NewJWT()
	nowTime := time.Now()
	expireTime := time.Duration(config.ServerConf.JWTExpire)
	claims := core.JWTCustomClaims{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
		StandardClaims: goJWT.StandardClaims{
			ExpiresAt: nowTime.Add(expireTime * time.Hour).Unix(),
			Issuer:    "clear",
		},
	}
	// 创建token
	token, err := jwtInstance.CreateToken(claims)
	if err != nil {
		return "", err
	}

	// 设置redis缓存
	const hourSecs int = 60 * 60
	core.RedisSet("TOKEN:"+user.Email, token, config.ServerConf.JWTExpire*hourSecs)
	return token, nil
}

// DestroyToken purge Token
func (as *AuthService) DestroyToken(email string) (bool, error) {
	return core.RedisDel("TOKEN:" + email)
}
