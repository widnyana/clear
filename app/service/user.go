package service

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/widnyana/clear/app/models"
	"gitlab.com/widnyana/clear/app/utils"
)

// UserService data mapper
type UserService struct{}

// QueryByEmail find user by email
func (u *UserService) QueryByEmail(email string) (user *models.UserModel, err error) {
	userModel := &models.UserModel{}
	condition := map[string]interface{}{
		"email": email,
	}
	user, err = userModel.FindOne(condition)
	return
}

// StoreUser save user information to database
func (u *UserService) StoreUser(username, email, password string, role int) (uid uint, err error) {
	log.Info().Msg("enter storeUser service")

	user := &models.UserModel{
		Email:    email,
		Username: username,
		Password: password,
		Status:   "ENABLE",
		Role:     role,
	}
	user.Password = utils.MakeSha256(user.Email + user.Password)
	uid, err = user.Insert()
	return

}
