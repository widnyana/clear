package controller

import (
	"github.com/gin-gonic/gin"
	//"github.com/creasty/defaults"
	"gitlab.com/widnyana/clear/app/utils"
	"gitlab.com/widnyana/clear/core"
)

type (
	// UserController provide action for interacting with user data
	UserController struct{}

	// UserEditRequest provide validation for update user request
	UserEditRequest struct {
		Name string `json:"name" binding:"max=20"`
		Role int    `json:"role" default:"1"`
	}

	// UserUpdatePasswordRequest provide validation for changing user password
	UserUpdatePasswordRequest struct {
		OldPassword     string `json:"old_password" binding:"required"`
		NewPassword     string `json:"new_password" binding:"required"`
		ConfirmPassword string `json:"confirm_password" binding:"required"`
	}
)

// Retrieve user claims
func (uc UserController) Retrieve(c *gin.Context) {
	claims := c.MustGet("claims").(*core.JWTCustomClaims)
	if claims != nil {
		utils.ResponseFormat(c, utils.Success, claims)
	}
}

// Update the user data
func (uc UserController) Update(c *gin.Context) {}

// UpdatePassword will update user password
func (uc UserController) UpdatePassword(c *gin.Context) {}
