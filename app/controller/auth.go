package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/widnyana/clear/app/service"
	"gitlab.com/widnyana/clear/app/utils"
	"gitlab.com/widnyana/clear/core"

	//"github.com/creasty/defaults"
	"github.com/go-sql-driver/mysql"
)

type (
	// AuthController ...
	AuthController struct{}

	// SignupRequest incoming payload
	SignupRequest struct {
		Email       string `json:"email" binding:"required,email"`
		AccountPass string `json:"password" binding:"required"`
		Username    string `json:"username" binding:"required"`
		Role        int    `json:"role" binding:""`
		ConfirmPass string `json:"confirm_pass" binding:"required"`
	}

	// SigninRequest ...
	SigninRequest struct {
		Email    string `json:"email" binding:"required,email"`
		Password string `json:"password" binding:"required,max=20"`
	}
)

// Signup handle create user
// @Summary register an account
// @Description Register user account with username, email, and password
// @Accept json
// @Produce json
// @Tags auth
// @ID auth.signup
// @Param body body SignupRequest true "Account registration request parameter"
// @Success 200 {string} json "{"status":200, "code": 2000001, msg:"Request processing succeeded"}"
// @Failure 400 {string} json "{"status":400, "code": 4000001, msg:"Request parameter is incorrect"}"
// @Failure 500 {string} json "{"status":500, "code": 5000001, msg:"Server internal error"}"
// @Router /auth/signup [post]
func (ac AuthController) Signup(c *gin.Context) {
	log.Info().Msg("enter signup controller")
	reqBody := SignupRequest{}
	if err := c.ShouldBindJSON(&reqBody); err != nil {
		log.Error().Msg(err.Error())
		utils.ResponseFormat(c, utils.RequestParamError, nil)
		return
	}

	if reqBody.AccountPass != reqBody.ConfirmPass {
		utils.ResponseFormat(c, utils.SignupPassUnmatch, nil)
		return
	}

	userService := service.UserService{}
	userID, err := userService.StoreUser(reqBody.Username, reqBody.Email, reqBody.ConfirmPass, reqBody.Role)
	if err != nil {
		log.Error().Msg(err.Error())
		mysqlErr, ok := err.(*mysql.MySQLError)
		if ok {
			if mysqlErr.Number == 1062 {
				utils.ResponseFormat(c, utils.SignupDuplicateUser, nil)
				return
			}
		}

		utils.ResponseFormat(c, utils.ServiceInsideError, nil)
		return
	}
	log.Info().Msgf("signup controller result userId: %d", userID)

	utils.ResponseFormat(c, utils.Success, map[string]uint{
		"user_id": userID,
	})
	return
}

// Signin handle user sign in
func (ac AuthController) Signin(c *gin.Context) {
	log.Info().Msg("enter Signin controller")
	reqBody := SigninRequest{}
	err := c.ShouldBindJSON(&reqBody)
	if err != nil {
		log.Error().Msg(err.Error())
		utils.ResponseFormat(c, utils.RequestParamError, nil)
		return
	}
	// find on database
	userService := service.UserService{}
	user, err := userService.QueryByEmail(reqBody.Email)
	if err != nil {
		log.Error().Msg(err.Error())
		utils.ResponseFormat(c, utils.ServiceInsideError, nil)
		return
	}
	if user == nil || user.Password != utils.MakeSha1(reqBody.Email+reqBody.Password) {
		utils.ResponseFormat(c, utils.SigninInfoError, nil)
		return
	}

	// Create Token
	authService := service.AuthService{
		User: user,
	}
	token, err := authService.GenerateToken(*user)
	if err != nil {
		utils.ResponseFormat(c, utils.ServiceInsideError, nil)
		return
	}

	utils.ResponseFormat(c, utils.Success, map[string]interface{}{
		"user_id":  user.ID,
		"username": user.Username,
		"email":    user.Email,
		"token":    token,
	})
	return
}

// Signout clear token
func (ac AuthController) Signout(c *gin.Context) {
	log.Info().Msg("enter signout controller")
	claims := c.MustGet("claims").(*core.JWTCustomClaims)
	log.Debug().Msgf("claims: %v", claims)

	// remove token
	authService := service.AuthService{}
	isOK, err := authService.DestroyToken(claims.Email)
	if err != nil || isOK == false {
		utils.ResponseFormat(c, utils.ServiceInsideError, nil)
		return
	}
	utils.ResponseFormat(c, utils.Success, map[string]interface{}{})
	return
}
