package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/widnyana/clear/app/service"
	"gitlab.com/widnyana/clear/app/utils"
	"gitlab.com/widnyana/clear/core"
)

type (
	// ShortenController provide action for shortening url
	ShortenController struct{}

	// ShortenCreateRequest provide validation for creating short URL
	ShortenCreateRequest struct {
		URL        string `json:"url" binding:"required"`
		CustomName string `json:"name"`
		OwnerID    uint
	}

	// ShortenGenericRequest provide validation for generic short URL Request
	ShortenGenericRequest struct {
		URL        string `json:"url"`
		CustomName string `json:"name"`
		OwnerID    uint   `json:"owner_id"`
	}
)

// Create short url
func (sc ShortenController) Create(c *gin.Context) {
	log.Info().Msg("enter create shortURL controller")
	body := ShortenCreateRequest{}
	if err := c.ShouldBindJSON(&body); err != nil {
		log.Error().Msg(err.Error())
		utils.ResponseFormat(c, utils.RequestParamError, nil)
		return
	}

	claims := c.MustGet("claims").(*core.JWTCustomClaims)

	svc := service.ShortenerService{}
	res, err := svc.Store(body.URL, body.CustomName, claims.ID)
	if err != nil {
		log.Error().Msg(err.Error())
		utils.ResponseFormat(c, utils.RequestParamError, err.Error())
		return
	}

	utils.ResponseFormat(c, utils.Success, res)
}

// Retrieve ...
func (sc ShortenController) Retrieve(c *gin.Context) {
	body := ShortenGenericRequest{}
	if err := c.ShouldBindJSON(&body); err != nil {
		log.Error().Msg(err.Error())
		utils.ResponseFormat(c, utils.RequestParamError, nil)
		return
	}

	svc := service.ShortenerService{}
	exist, res := svc.Retrieve(body.CustomName, body.URL, body.OwnerID)
	if exist == true {
		utils.ResponseFormat(c, utils.Success, res)
		return
	}

	utils.ResponseFormat(c, utils.DataNotFound, nil)
}

// Delete short url
func (sc ShortenController) Delete(c *gin.Context) {
	name := c.Param("name")
	if len(name) < 1 {
		utils.ResponseFormat(c, utils.RequestParamError, "Invalid URL Identifier")
		return
	}

	claims := c.MustGet("claims").(*core.JWTCustomClaims)
	svc := service.ShortenerService{}
	err := svc.Delete(name, claims.ID)
	if err != nil {
		utils.ResponseFormat(c, utils.RequestParamError, err.Error())
		return
	}

	utils.ResponseFormat(c, utils.Success, nil)
}

// Detail return information for short code
func (sc ShortenController) Detail(c *gin.Context) {

}
