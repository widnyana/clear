package utils

import (
	"net/http"
)

// Code Error output data structure
type Code struct {
	Status  int    `json:"status"`  // HTTP status
	Code    int    `json:"code"`    // business error code
	Message string `json:"message"` // Business error message
}

var (
	// Success request processed successfully
	Success = &Code{http.StatusOK, 2000001, "Request processing succeeded"}

	// RequestParamError request parameter error
	RequestParamError = &Code{http.StatusBadRequest, 4000001, "Request parameter is incorrect"}

	// AccountPassUnmatch The original password of the account does not match
	AccountPassUnmatch = &Code{http.StatusBadRequest, 4000002, "The original password of the account does not match"}

	// SignupPassUnmatch registration twice enter password does not match
	SignupPassUnmatch = &Code{http.StatusBadRequest, 4000003, "Register Password does not match"}

	// SignupDuplicateUser registration duplicate unique index
	SignupDuplicateUser = &Code{http.StatusBadRequest, 4000003, "Duplicate data"}

	// AccountNameExist account nickname is duplicated, please change to another nickname
	AccountNameExist = &Code{http.StatusBadRequest, 4000004, "Account nickname is already in use"}

	// UploadSuffixError This upload file format is currently not supported.
	UploadSuffixError = &Code{http.StatusBadRequest, 4000005, "This upload file format is currently not supported"}

	// UploadSizeLimit currently supports only file content less than 5M
	UploadSizeLimit = &Code{http.StatusBadRequest, 4000006, "The current upload only supports file content less than 5M"}

	// SigninInfoError account name or password is incorrect
	SigninInfoError = &Code{http.StatusUnauthorized, 4010001, "The account name or password is incorrect"}

	// TokenNotFound request does not carry a token, no access
	TokenNotFound = &Code{http.StatusUnauthorized, 4010002, "Request not carrying Token, no access"}

	// TokenInvalid invalid Token information
	TokenInvalid = &Code{http.StatusUnauthorized, 4010003, "Invalid Token Information"}

	// DataNotFound no data found
	DataNotFound = &Code{http.StatusNotFound, 4040001, "Data Not Found"}

	// ServiceInsideError server internal error
	ServiceInsideError = &Code{http.StatusInternalServerError, 5000001, "Server Internal Error"}
)
