package utils

import ("fmt")

// EncryptPassword make password unreadable
func EncryptPassword(email, password string) string {
	return MakeSha512(fmt.Sprintf("%s:%s", email, password))
}