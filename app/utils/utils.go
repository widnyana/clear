package utils

import (
	"crypto/sha1"
	"crypto/sha512"
	"crypto/sha256"
	"encoding/hex"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"os"
)

// ResponseFormat Return data formatting
func ResponseFormat(c *gin.Context, respStatus *Code, data interface{}) {
	if respStatus == nil {
		log.Error().Msg("response status param not found!")
		respStatus = RequestParamError
	}
	c.JSON(respStatus.Status, gin.H{
		"code": respStatus.Code,
		"msg":  respStatus.Message,
		"data": data,
	})
	return
}

// MakeSha1 Calculate the sha1 hash value of a string
func MakeSha1(source string) string {
	sha1Hash := sha1.New()
	sha1Hash.Write([]byte(source))
	return hex.EncodeToString(sha1Hash.Sum(nil))
}

// IsExist Determine if a file or path exists
func IsExist(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// IsPerm Check if the file or path has permission
func IsPerm(path string) bool {
	_, err := os.Stat(path)
	return os.IsPermission(err)
}


// MakeSha512 create SHA512 from string
func MakeSha512(source string) string {
	hasher := sha512.New()
	hasher.Write([]byte(source))
	return hex.EncodeToString(hasher.Sum(nil))
}

// MakeSha256 create SHA256 from string
func MakeSha256(source string) string {
	hasher := sha256.New()
	hasher.Write([]byte(source))
	return hex.EncodeToString(hasher.Sum(nil))
}