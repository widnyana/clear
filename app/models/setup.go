package models

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" // mysql dialect
	"gitlab.com/widnyana/clear/config"
)

// DB contain mysql connection
var DB *gorm.DB

// Setup configure gorm
func Setup() error {
	var err error
	conString := config.MysqlConf.CreateDSN()

	DB, err = gorm.Open("mysql", conString)
	if err != nil {
		fmt.Printf("mysql connect error %v", err)
		time.Sleep(10 * time.Second)
		DB, err = gorm.Open("mysql", conString)
		if err != nil {
			return err
		}
	}

	if DB.Error != nil {
		fmt.Printf("database error %v", DB.Error)
	}

	DB.LogMode(config.MysqlConf.DebugQuery)
	DB.SingularTable(true)
	DB.DB().SetMaxIdleConns(config.MysqlConf.MaxIdleConn)
	DB.DB().SetMaxOpenConns(config.MysqlConf.MaxOpenConn)

	migrate(DB)
	return nil
}

func migrate(db *gorm.DB) {
	db.Set(
		"gorm:table_options",
		"ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci",
	).AutoMigrate(&UserModel{}, &URLModel{})
	db.Model(&UserModel{}).AddUniqueIndex("uk_email", "email")
}
