package models

import (
	"github.com/jinzhu/gorm"
)

// UserModel for table users
type UserModel struct {
	gorm.Model
	Username string `gorm:"column:name;type:varchar(100);unique_index;default:null"`
	Password string `gorm:"column:password;type:varchar(100);default:null"`
	Email    string `gorm:"column:email;type:varchar(100);unique_index;default:null"`
	Avatar   string `gorm:"column:avatar;type:varchar(100);default:null"`
	Status   string `sql:"type:ENUM('ENABLE', 'DISABLE')"`
	Role     int    `gorm:"column:role;type:tinyint;default:1"`
}

// TableName provide table name definition
func (user UserModel) TableName() string {
	return "user_account"
}

// Insert new user to database
func (user *UserModel) Insert() (userID uint, err error) {

	result := DB.Create(&user)
	userID = user.ID
	if result.Error != nil {
		err = result.Error
	}
	return
}

// FindOne query for one user
func (user *UserModel) FindOne(condition map[string]interface{}) (*UserModel, error) {
	var userInfo UserModel
	result := DB.Select("id, name, email, avatar, password").Where(condition).First(&userInfo)
	if result.Error != nil && result.Error != gorm.ErrRecordNotFound {
		return nil, result.Error
	}
	if userInfo.ID > 0 {
		return &userInfo, nil
	}
	return nil, nil
}

// FindAll return all users paginated
func (user *UserModel) FindAll(pageNum int, pageSize int, condition interface{}) (users []UserModel, err error) {

	result := DB.Offset(pageNum).Limit(pageSize).Select("id", "name", "email").Where(condition).Find(&users)
	err = result.Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return
	}
	return
}

// UpdateOne data of user
func (user *UserModel) UpdateOne(userID uint, data map[string]interface{}) (*UserModel, error) {
	err := DB.Model(&UserModel{}).Where("id = ?", userID).Updates(data).Error
	if err != nil {
		return nil, err
	}
	var updUser UserModel
	err = DB.Select([]string{"id", "name", "email", "avatar"}).First(&updUser, userID).Error
	if err != nil {
		return nil, err
	}
	return &updUser, nil
}

// DeleteOne user record
func (user *UserModel) DeleteOne(userID uint) (delUser UserModel, err error) {
	if err = DB.Select([]string{"id"}).First(&user, userID).Error; err != nil {
		return
	}

	if err = DB.Delete(&user).Error; err != nil {
		return
	}
	delUser = *user
	return
}
