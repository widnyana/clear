package models

import (
	"github.com/jinzhu/gorm"
)

// URLModel Contains URL
type URLModel struct {
	gorm.Model
	ShortURL string `gorm:"size:255;not null;unique_index"`
	URL      string `gorm:"size:255;not null"`
	OwnerID  uint   `gorm:"index:user_id"`
}

// TableName provide table name definition
func (u URLModel) TableName() string {
	return "urls"
}
