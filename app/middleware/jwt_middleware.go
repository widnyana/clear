package middleware

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gitlab.com/widnyana/clear/app/utils"
	"gitlab.com/widnyana/clear/core"
)

var (
	stripFromJwt = []string{
		"Bearer ",
		"Token ",
	}
)

// JWTAuthMiddleware handle authentication before request handled
func JWTAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")
		if token == "" {
			utils.ResponseFormat(c, utils.TokenNotFound, nil)
			c.Abort()
			return
		}
		token = cleanJWTToken(token)

		jwtInstance := core.NewJWT()
		claims, err := jwtInstance.ParseToken(token)
		if err != nil {
			log.Error().
				Str("token", token).
				Str("error", err.Error()).
				Msg("got error parsing token")
			utils.ResponseFormat(c, utils.TokenInvalid, nil)
			c.Abort()
			return
		}

		// check for cache
		tokenCache, err := core.RedisGet(fmt.Sprintf("TOKEN:%s", claims.Email))
		if err != nil {
			utils.ResponseFormat(c, utils.ServiceInsideError, nil)
			c.Abort()
			return
		}

		// if cache != token, token was expired
		if token != tokenCache {
			log.Error().
				Str("token", token).
				Str("error", tokenCache).
				Msg("token was expired")
			utils.ResponseFormat(c, utils.TokenInvalid, nil)
			c.Abort()
			return

		}

		log.Debug().Str("Auth User", claims.Username).Msg("")

		c.Set("claims", claims)
		c.Next()
	}
}

func cleanJWTToken(token string) (clean string) {

	for _, key := range stripFromJwt {
		if strings.Contains(token, key) {
			clean = strings.Replace(token, key, "", 1)
		}
	}

	return clean
}
