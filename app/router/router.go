package router

import (
	conf "gitlab.com/widnyana/clear/config"
	"gitlab.com/widnyana/clear/app/controller"

	"time"

	"gitlab.com/widnyana/clear/app/middleware"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// InitRouter initialize the router
func InitRouter() *gin.Engine {
	gin.SetMode(conf.ServerConf.RunMode)

	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  conf.CORSConf.AllowAllOrigins,
		AllowMethods:     conf.CORSConf.AllowMethods,
		AllowHeaders:     conf.CORSConf.AllowHeaders,
		ExposeHeaders:    conf.CORSConf.ExposeHeaders,
		AllowCredentials: conf.CORSConf.AllowCredentials,
		MaxAge:           conf.CORSConf.MaxAge * time.Hour,
	}))

	// // swagger
	// config := &ginSwagger.Config{
	// 	URL: fmt.Sprintf("http://%s/swagger/doc.json", bind), //The url pointing to API definition
	// }
	// r.GET("/swagger/*any", ginSwagger.CustomWrapHandler(config, swaggerFiles.Handler))

	apiV1 := r.Group("api/v1")
	authController := new(controller.AuthController)
	{
		apiV1.POST("/auth/signup", authController.Signup)
		apiV1.POST("/auth/signin", authController.Signin)
	}

	apiV1.Use(middleware.JWTAuthMiddleware())
	{
		apiV1.POST("/auth/signout", authController.Signout)

		userCtrl := new(controller.UserController)
		apiV1.GET("/user", userCtrl.Retrieve)

		shortenCtrl := new(controller.ShortenController)
		apiV1.GET("/url/exist", shortenCtrl.Retrieve)
		apiV1.POST("/url", shortenCtrl.Create)
		apiV1.DELETE("/url/:name", shortenCtrl.Delete)
	}

	// 	userController := new(v1.UserController)
	// 	apiV1.Use(middleware.JWTAuth())
	// 	{
	// 		// 账户注销
	// 		apiV1.POST("/auth/signout", authController.Signout)
	// 		// 查看用户信息
	// 		apiV1.GET("/user", userController.Retrieve)
	// 		// 修改用户名称
	// 		apiV1.PATCH("/user/name", userController.AlterName)
	// 		// 修改用户密码
	// 		apiV1.PATCH("/user/pass", userController.AlterPass)
	// 		// 修改用户头像
	// 		apiV1.PATCH("/user/avatar", userController.AlterAvatar)

	// 		taskController := v1.TaskController{}

	// 		// 获取任务列表
	// 		apiV1.GET("/task", taskController.List)
	// 		// 新增任务
	// 		apiV1.POST("/task", taskController.Create)
	// 		// 获取任务详情
	// 		apiV1.GET("/task/:taskId", taskController.Retrieve)
	// 		// 修改任务参数
	// 		apiV1.PUT("/task/:taskId", taskController.Update)
	// 		// 删除任务
	// 		apiV1.DELETE("/task/:taskId", taskController.Destroy)

	// 	}

	// }
	return r
}
