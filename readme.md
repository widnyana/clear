Clear
-----

stupid url shortener service

# Requirements

- mysql
- redis


# How to

```bash
make clean vendor
make build
cp clear.example.toml clear.toml  # edit as needed
./bin/clear-server
```



# License

[MIT License](https://widnyana.mit-license.org/2019)