package config

import (
	"bytes"
	"time"
)

type (
	server struct {
		Debug        bool          `mapstructure:"debug"`
		RunMode      string        `mapstructure:"run_mode"`
		Port         int           `mapstructure:"port"`
		Bind         string        `mapstructure:"bind"`
		ReadTimeout  time.Duration `mapstructure:"read_timeout"`
		WriteTimeout time.Duration `mapstructure:"write_timeout"`
		JWTSecret    string        `mapstructure:"jwt_secret"`
		JWTExpire    int           `mapstructure:"jwt_expire"`
	}

	cors struct {
		AllowAllOrigins  bool          `mapstructure:"allow_all_origins"`
		AllowMethods     []string      `mapstructure:"allow_methods"`
		AllowHeaders     []string      `mapstructure:"allow_headers"`
		ExposeHeaders    []string      `mapstructure:"expose_headers"`
		AllowCredentials bool          `mapstructure:"allow_credentials"`
		MaxAge           time.Duration `mapstructure:"max_age"`
	}

	mysql struct {
		DebugQuery      bool          `mapstructure:"debug_query"`
		User            string        `mapstructure:"user"`
		Password        string        `mapstructure:"password"`
		Host            string        `mapstructure:"host"`
		Protocol        string        `mapstructure:"protocol"`
		Port            string        `mapstructure:"port"`
		Database        string        `mapstructure:"database"`
		MaxOpenConn     int           `mapstructure:"max_open_conn"`
		MaxIdleConn     int           `mapstructure:"max_idle_conn"`
		ConnMaxLifetime time.Duration `mapstructure:"conn_lifetime"`
		ParseTime       bool          `mapstructure:"parse_time"`
		ReadTimeout     time.Duration `mapstructure:"read_timeout"`
		WriteTimeout    time.Duration `mapstructure:"write_timeout"`
		Timeout         time.Duration `mapstructure:"timeout"`
		Params          []string      `mapstructure:"params"`
	}

	redis struct {
		Host        string        `mapstructure:"host"`
		Port        int           `mapstructure:"port"`
		Password    string        `mapstructure:"password"`
		DBNum       int           `mapstructure:"db"`
		MaxIdle     int           `mapstructure:"max_idle"`
		MaxActive   int           `mapstructure:"max_active"`
		IdleTimeout time.Duration `mapstructure:"idle_timeout"`
	}

	logger struct {
		Level  string `mapstructure:"level"`
		Pretty bool   `mapstructure:"pretty"`
		Color  bool   `mapstructure:"color"`
	}
)

var (
	// ServerConf hold http server configuration
	ServerConf = new(server)

	// CORSConf hold CORS configuration
	CORSConf = new(cors)

	// MysqlConf hold mysql client connection info
	MysqlConf = new(mysql)

	// RedisConf hold redis client connection info
	RedisConf = new(redis)

	// LoggerConf contains data should be passed to logger instance
	LoggerConf = new(logger)
)

func (m *mysql) CreateDSN() string {
	var buf bytes.Buffer
	hasParam := false

	// [username[:password]@]
	if len(m.User) > 0 {
		buf.WriteString(m.User)
		if len(m.Password) > 0 {
			buf.WriteByte(':')
			buf.WriteString(m.Password)
		}
		buf.WriteByte('@')
	}

	// [protocol[(address)]]
	if len(m.Protocol) > 0 {
		buf.WriteString(m.Protocol)
		if len(m.Host) > 0 {
			buf.WriteByte('(')
			buf.WriteString(m.Host)
			if len(m.Port) > 0 {
				buf.WriteByte(':')
				buf.WriteString(m.Port)
			}
			buf.WriteByte(')')
		}
	}

	// /dbname
	buf.WriteByte('/')
	buf.WriteString(m.Database)

	if m.ParseTime {
		if hasParam {
			buf.WriteString("&parseTime=true")
		} else {
			hasParam = true
			buf.WriteString("?parseTime=true")
		}
	}

	if m.ReadTimeout > 0 {
		if hasParam {
			buf.WriteString("&readTimeout=")
		} else {
			hasParam = true
			buf.WriteString("?readTimeout=")
		}
		buf.WriteString(m.ReadTimeout.String())
	}

	if m.Timeout > 0 {
		if hasParam {
			buf.WriteString("&timeout=")
		} else {
			hasParam = true
			buf.WriteString("?timeout=")
		}
		buf.WriteString(m.Timeout.String())
	}

	if m.WriteTimeout > 0 {
		if hasParam {
			buf.WriteString("&writeTimeout=")
		} else {
			hasParam = true
			buf.WriteString("?writeTimeout=")
		}
		buf.WriteString(m.WriteTimeout.String())
	}

	return buf.String()
}
