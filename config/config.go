package config

import (
	"github.com/spf13/viper"
)

var config *viper.Viper

// LoadConfig from defined configuration directory
func LoadConfig() (err error) {
	config = viper.New()

	config.SetConfigName("clear")
	config.AddConfigPath("$HOME/.damarseta")
	config.AddConfigPath("/etc/damarseta")
	config.AddConfigPath(".")
	config.SetConfigType("toml")
	err = config.ReadInConfig()
	if err != nil {
		return err
	}

	config.UnmarshalKey("server", ServerConf)
	config.UnmarshalKey("mysql", MysqlConf)
	config.UnmarshalKey("redis", RedisConf)
	config.UnmarshalKey("logger", LoggerConf)
	config.UnmarshalKey("cors", CORSConf)

	return nil
}

// GetConfig provice access to configuration instance
func GetConfig() *viper.Viper {
	return config
}
