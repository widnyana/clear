package main

import (
	"fmt"
	"log"

	"gitlab.com/widnyana/clear/app/models"
	"gitlab.com/widnyana/clear/app/router"
	"gitlab.com/widnyana/clear/config"
	"gitlab.com/widnyana/clear/core"
)

func init() {
	var err error
	err = config.LoadConfig()
	if err != nil {
		log.Fatalf("Fail loading configuration: %s", err.Error())
	}

	core.SetupLogger()
	err = models.Setup()
	if err != nil {
		log.Fatalf("Could not connect to mysql server: %s", err.Error())
	}

	err = core.RedisSetup()
	if err != nil {
		log.Fatalf("Could not connect to redis server: %s", err.Error())
	}
	core.SetupValidator()
}

func main() {
	fmt.Println("Welcome to clear")

	router := router.InitRouter()
	bind := fmt.Sprintf("%s:%d", config.ServerConf.Bind, config.ServerConf.Port)
	log.Printf("Running on: %s", bind)
	router.Run(bind)
}
